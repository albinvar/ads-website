<?php

require_once('config.php');

$id = empty($_GET['id']) ? null : $_GET['id'];

function make_query($conn, $id)
{
    $query = "SELECT * FROM images WHERE ad_id = {$id}";
    $result = mysqli_query($conn, $query);

    return $result;
}
function make_slide_indicators($conn, $id)
{
    $output = '';
    $count = 1;
    $result = make_query($conn, $id);
    while ($row = mysqli_fetch_array($result)) {
        if ($count == 1) {
            $output .= '
   <li data-target="#dynamic_slide_show" data-slide-to="'.$count.'" class="active"></li>
   ';
        } else {
            $output .= '
   <li data-target="#dynamic_slide_show" data-slide-to="'.$count.'"></li>
   ';
        }
        $count = $count + 1;
    }
    return $output;
}

function make_slides($conn, $id)
{
    $output = '';
    $count = 1;
    $result = make_query($conn, $id);
$total_img=mysqli_num_rows($result);
    while ($row = mysqli_fetch_array($result)) {
        if ($count == 1) {
            $output .= '<div class="item active">';
        } else {
            $output .= '<div class="item">';
        }
        
        $output .= '
   <img src="ads_images/'.$row["name"].'" alt="'.$row["id"].'" />
   <div class="carousel-caption">
    <h3>'.$count.'/'.$total_img.'</h3>
   </div>
  </div>
  ';
        $count = $count + 1;
    }
    return $output;
}function make_slides_ads($conn, $id)
{
    $output = '';
    $query = "SELECT * FROM ads WHERE id={$id}";
    $result = mysqli_query($conn, $query);
  $result2 = mysqli_fetch_array($result); 
    $output .='<font color=black size=5pt >'.$result2['title'].' <font color=black size=5pt>'.$result2['description'].'<p><br>Утас: <a href="tel:+'.$result2['phone'].'"><font color=Green size=5pt>+'.$result2['phone'].'</a> </p>
    <br><p><font color=black size=5pt>Үнэ: '.$result2['price'].'  <font size =2pt>Нийтэлсэн огноо:'.$result2['created_at'].' </p>'; 
    return $output;
}
$query = "SELECT * FROM ads WHERE id={$id}";
    $result = mysqli_query($conn, $query);
  $result2 = mysqli_fetch_array($result); 
?>
<!DOCTYPE html>
<html>
 <head>
  <title>Image Preview</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/mn_MN/sdk.js#xfbml=1&version=v10.0&appId=227992385531420&autoLogAppEvents=1" nonce="9sErtKUs"></script>
   </head>
 
 <body>

  <br />
  
  <div class="container">
   <h2 align="center">PreviewAd Images </h2>
   <div class="fb-share-button" data-href="http://uvszar.com/slider.php?id=<?=$id?>" data-layout="button_count" data-size="100%"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Хуваалцах</a></div>
 <p>  <?php echo make_slides_ads($conn, $id); ?> <p>
   <br /> <p> <a class="button" href="categore.php">Go Back</button></p>
   
   <div id="dynamic_slide_show" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
  
    <?php echo make_slide_indicators($conn, $id); ?>
    </ol>

    <div class="carousel-inner">
     <?php echo make_slides($conn, $id); ?>
    </div>
    <a class="left carousel-control" href="#dynamic_slide_show" data-slide="prev">
     <span class="glyphicon glyphicon-chevron-left"></span>
     <span class="sr-only">Previous</span>
    </a>

    <a class="right carousel-control" href="#dynamic_slide_show" data-slide="next">
     <span class="glyphicon glyphicon-chevron-right"></span>
     <span class="sr-only">Next</span>
    </a>

   </div>
  </div>
 </body>
</html>

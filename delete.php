<?php

session_start();
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

require_once('config.php');

$id = empty($_GET['id']) ? null : $_GET['id'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id = $_POST['id'];
    
    $code = $_POST['code'];

    $stmt = $conn->prepare("SELECT id, secret_code FROM ads WHERE id = ? LIMIT 1");
    $stmt->bind_param('s', $id);
    $stmt->execute();
    $result = $stmt->get_result();
    $ad = $result->fetch_object();
    $ad_id = $ad->id;
    $hashedCode = $ad->secret_code;
    
    if (password_verify($code, $hashedCode)) {
        deleteImages($conn, $ad_id);  // this ad_id is not Images table's ad_id Column
        
        $stmt = $conn->prepare("DELETE FROM ads WHERE id = ?");
        $stmt->bind_param('s', $id);
        if ($stmt->execute()) {
            echo '<script language = "javascript">;
       alert("Deleted Successfully");
  window.location = "categore.php";
     </script>';
        } else {
            echo '<script language = "javascript">;
       alert("An error encountered while processing your request");
  window.location = "delete.php?id='. $id .'";
     </script>';
        }
    } else {
        echo '<script language = "javascript">;
       alert("Wrong Secret Code");
  window.location = "delete.php?id='. $id .'";
     </script>';
    }
} elseif (isset($_GET['id']) && !empty($id)) {
    $stmt = $conn->prepare("SELECT secret_code FROM ads WHERE id = ? LIMIT 1");
    $stmt->bind_param('s', $id);
    $stmt->execute();
    $result = $stmt->get_result();
    $ad = $result->fetch_object();
    
    if (is_null($ad)) {
        die("No Advertisement Found with this id");
    }
} else {
    die('No Id Found in URL');
}
  
//functions
  
function deleteImages($conn, $id)
{
    $stmt = $conn->prepare("SELECT id,name FROM images WHERE ad_id=?");
    $stmt->bind_param('s', $id);
    $stmt->execute();
    $result = $stmt->get_result();
    $images = $result->fetch_all();
    
    foreach ($images as $image) {
        $path = "ads_images/" .$image[1];
        unlink($path);
        deleteImagesTable($conn, $image[0]);
    }
}

function deleteImagesTable($conn, $id)
{
    $stmt = $conn->prepare("DELETE FROM images WHERE id = ?");
    $stmt->bind_param('s', $id);
    if ($stmt->execute()) {
        return true;
    }
}
  
?>

  <?php require('includes/header.php'); ?>
  
				<!-- Large modal -->
				<script>
				$('#myModal').modal('');
				</script>



	<div class="main-banner banner text-center">
	  <div class="container">
         
    <p>	<a href="post-ad.php">Энд дарж зараа оруулна уу</a> </p>
	  </div>
	</div>

   	<!-- Submit Ad -->
     
 

  <div class="submit-ad main-grid-border">
		<div class="container">
			<h2 class="head">Delete Advertisement</h2>   <a href="index.php" class="button">Go Back</a>
			<div class="post-ad-form">
<form class="" action="delete.php" method="POST">
  <div class="clearfix"></div>
  <label>Secret Code<span>*</span></label>
  <input type="text" name="code" placeholder="Enter your secret code" required>
  <input type="hidden" name="id" value="<?= htmlspecialchars($id); ?>" required>
<input type="submit" name="submit" value="Delete"/>
</form>
	<p class="post-terms"> Deleting the post is an irreversible process. Please be aware..</p>
	</div>
					<div class="clearfix"></div>
						<script src="js/filedrag.js"></script>

  </div>

					<div class="clearfix"></div>

					</div>
          

	<!-- // Submit Ad -->
	<!-- Reclam image -->
  <div class="container">
				  
          <img src="images/footer_bann.jpg" alt="uvszar"> 
          </div>	
      
        <div class="clearfix"></div>
        
          </div>
<?php require('includes/footer.php'); ?>